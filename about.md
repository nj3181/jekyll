---
layout: page
title: About Us
---

We are a local ATV Club dedicated to providing an area for recreational ATV riding.

Right now, our trail system ranges from railroad beds to tight, technical woods
trails. The trails stretch from Altmar to Parish to Orwell and Pulaski, for 90 miles
of riding. We have over 1000 members from all over the area. We have meetings every
third Saturday of each month from March through October. If you are interested in
joining, you can submit an application. 

[Get the Registration Form now!](../resources/regForm.pdf) 

## Officers
* **President:** Steve Cronk
  * 315-298-3312
* **Vice President:** Brad Hilton
  * 315-480-5104
* **Secretary:** Mary Gaffey
  * 315-298-5444
* **Treasurer:** Claudia Gamble
  * 315-298-4829
 
## Board of Directors
* Jr. O'Neil	
  * 315-668-8995
* Mike Ortman	
  * 315-761-2423
* Todd Pfluger	
  * 315-298-3169

## Trail Bosses
* Mike Ortman, **Trail Coordinator**
  * 315-761-2423
* Dave Gaffey
  * 315-298-5444
* Bobby Eggerling
  * 315-668-6672
* John McGrew
  * 315-387-5415

## Miscellaneous
* Becky Hilton, **Membership Chair**
  * 315-298-6081
* Jen Pieropan, **Newsletter Editor**
  * 315-625-7456
* Nathan Johnson, **Webmaster**	
  * 585-723-9362
* Bert Cronk, **Campground Manager**
  * 315-298-4736
* Lynn Sorbello, **Merchandise**
  * 315-243-9748
* Mary Gaffey, **Girls Club**
  * 315-298-5444
* Jr. O'Neill, **Equipment Mechanic**
  * 315-668-8995

## Additional Info 
OCATV Club has joined the Lewis County ATV Association. This will allow our club members to purchase membership in the Lewis County Permit system at a reduced cost for clubs in the organization. Anyone wishing to ride our clubs trails MUST be a member of OCATV Club. The Lewis County permit alone doesn't give access to OCATV trails.

### We now have T-Shirts, Sweatshirts and Hats!
Call Lynn Sorbello at 315-243-9748 for more information. The T-shirts will have our logo on them and "**For a good feeling go four wheeling with the Oswego County ATV Club**"
